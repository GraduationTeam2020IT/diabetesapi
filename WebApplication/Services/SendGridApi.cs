﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Services
{
    public static class SendGridApi
    {
      public  static async Task<bool> Execute(string userEmail,string userName,string plainTextContent,string htmlContent,string subject)
        {
            var apiKey = "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";
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("test@example.com", "Diabetessite");
            
            var to = new EmailAddress(userEmail, userName);
  
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            var response = await client.SendEmailAsync(msg);

            return await Task.FromResult(true); 
        }
    }
}
