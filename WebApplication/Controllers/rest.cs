﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models;
using WebApplication.ModelViews;
using WebApplication.Repo;

namespace WebApplication.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class restController : ControllerBase
    {
        private int user_id;
        private UserManager<ApplicationUser> manager;
        private IDiabetes diabetes;
        private DiabetesSystem2Context db;
        public restController(UserManager<ApplicationUser> _manager, IDiabetes _diabetes, DiabetesSystem2Context _db)
        {
            manager = _manager;
            diabetes = _diabetes;
            db = _db;

        }
        private void GetUserId()
        {
            user_id = diabetes.GetUserID(manager.GetUserId(HttpContext.User));
        }


        [HttpGet]
        [Route("GetPatientPoints")]
        [Authorize(Roles = "Patient")]
        public IActionResult GetPatientPoints()
        {
            GetUserId();
            Patient p = diabetes.GetPatient(user_id);
            short points;
            if (p.Points != null)
            {
                points = p.Points.Value;
            }
            else
            {
                points = 0;
            }
            return Ok(points);
        }

        [Route("ctg")]
        ///GetAllCategories
        public IActionResult ctg()
        {
            var c = diabetes.GetAllCategories();
            return Ok(c);
        }


        [HttpGet]
        [Route("MedicalInfo")]
        [Authorize(Roles = "Patient")]

        public IActionResult GetMedicalInfo()
        {
            GetUserId();
            return Ok(diabetes.GetPatient(user_id));
        }
        [HttpPost]
        [Route("MedicalInfo")]
        [Authorize(Roles = "Patient")]

        public IActionResult UpdateMedicalInfo([FromBody] Patient patient)
        {
            GetUserId();
            diabetes.UpdatePatient(patient);
            return Ok(patient);
        }
        [HttpGet]
        [Route("A1C")]
        [Authorize(Roles = "Patient")]

        public IActionResult GetLastA1C()
        {
            GetUserId();
            short A1Ctype = 0;
            diabetes.GetAllTests(user_id, A1Ctype);
            return Ok(diabetes.GetAllTests(user_id, A1Ctype).LastOrDefault());


        }

        [HttpPost]
        [Route("A1C/{result}/{medication}")]
        [Authorize(Roles = "Patient")]


        public IActionResult AddTest(int result, bool medication)
        {
            GetUserId();
            short A1Ctype = 0;
            //   string ID =User.Claims.First(i => i.Type == "UserId").Value;
          var s=  diabetes.AddTest(result, A1Ctype, user_id, medication);
            return Ok(s);
        }

        [HttpGet]
        [Route("LastCheckup")]
        [Authorize(Roles = "Patient")]


        public IActionResult GetLastCheckup()
        {
            GetUserId();
            return Ok(diabetes.GetPatientCheckups(user_id).LastOrDefault());
        }

        [HttpGet]
        [Route("Checkups")]
        [Authorize(Roles = "Patient")]

        public IActionResult GetAllCheckups()
        {
            GetUserId();
            return Ok(diabetes.GetPatientCheckups(user_id));
        }

        [HttpPost]
        [Route("Checkups")]
        [Authorize(Roles = "Patient")]

        public IActionResult AddCheckup([FromBody] ChecksUps checksUp)
        {
            GetUserId();
           var x=  diabetes.AddCheckup(checksUp);
            return Ok(x);
        }
        [HttpPut]
        [Route("Checkups")]
        [Authorize(Roles = "Patient")]

        public IActionResult UpdateCheckup([FromBody] ChecksUps checksUp)
        {
            GetUserId();
            if (checksUp.PatientId == user_id)
            {
                return Ok(diabetes.UpdateCheckup(checksUp));
            }
            return BadRequest();
        }

        [HttpGet]
        [Route("Tests")]
        [Authorize(Roles = "Patient")]

        public IActionResult GetTests()
        {
            GetUserId();
            return Ok(diabetes.GetAllTests(user_id));
        }

        [Route("react")]
        public IActionResult getReacts()
        {
            var c = diabetes.GetAllReacts();
            return Ok(c);
        }




        [HttpGet("{doctorid}")]
        [Route("unfollowdr/{doctorid}")]
        [Authorize(Roles = "Patient")]

        public IActionResult unfollow_dr(int doctorid) //11 / 14
        {
            GetUserId();

            var n = diabetes.UnfollowDoctor(user_id, doctorid);
            return Ok(n);
        }

        [HttpPost("{followID}/{status}")]
        [Route("access_medinfo/{status}")] //2 / 0
        [Authorize(Roles = "Patient")]
        ////////////////////////////////may need modification
        public IActionResult update_access_to_medInfo([FromBody] int followID, short status)
        { 
            //status = 0 rejected 2 approved
            var n = diabetes.Update_access_medicalInfo(followID, status);
            return Ok(n);
        }

        //-------------------------------------------------------------------------------------------------------

        [HttpGet]
        [Route("allPosts")]
        [AllowAnonymous]
        public IActionResult all_posts()
        {
            var n = diabetes.GetAllPosts();
            return Ok(n);
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///


        //1 get dr info
        [HttpGet]
        [Route("doctor")]
        [Authorize(Roles = "Doctor")]

        public IActionResult getDrInfo()
        {
             GetUserId();
            DoctorModel doctorinfo = new DoctorModel();
            Users u = diabetes.GetUser(user_id);

            doctorinfo.PhoneNumber = manager.Users.Where(a => a.Id == u.ID).Select(a => a.PhoneNumber).FirstOrDefault();



            doctorinfo.UserName = u.UserName;
            doctorinfo.ImageSource = u.ImageSource;
            doctorinfo.Email = manager.Users.Where(a => a.Id == u.ID).Select(a => a.Email).FirstOrDefault();
            doctorinfo.Password = manager.Users.Where(a => a.Id == u.ID).Select(a => a.PasswordHash).FirstOrDefault();
            Doctor doctor = diabetes.GetDoctor(user_id);
            doctorinfo.Certificates = diabetes.GetCertificates(user_id);
            doctorinfo.Address = doctor.Address;
            doctorinfo.ValidationStatus = doctor.ValidationStatus;

            return Ok(doctorinfo);


        }
        //2 update dr info
        //[HttpPut]
        //[Route("drupdate")]
        //public IActionResult updateDrInfo(Doctor doctor)
        //{
        //    //Users u = d.GetUser(doctor.DoctorId);
        //    var n = diabetes.UpdateDoctor(doctor);
        //    return Ok(n);
        //}
        //3 get all following patients
        [HttpGet]
        [Route("getpatients")]
        [Authorize(Roles = "Doctor")]

        public IActionResult getAllFollowingPatients()
        {
            GetUserId();
            var n = diabetes.GetMyPatients(user_id);
            return Ok(n);
        }
        //4 request access for medical info
        [HttpPost]
        [Route("request/{pid}")]
        [Authorize(Roles = "Doctor")]

        public IActionResult request_access_medicalInfo( int pid,[FromBody] int FollowID) //gives error in function
        {
            GetUserId();

            var n = diabetes.request_access_medicalInfo(user_id, pid, FollowID);
            //var x = db.PatientDoctorsFollow.Where(a => a.Id == FollowID).FirstOrDefault();
            //var y = db.PatientDoctorsFollow.Find(FollowID);
            if (n != null)

            return Ok(n);
            return BadRequest();
        }
        //5 get dr posts


        //6 get dr questions


    }
}
